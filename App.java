import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.prefs.*;

import com.ibm.cloud.sdk.core.service.security.IamOptions;
import com.ibm.watson.speech_to_text.v1.SpeechToText;
import com.ibm.watson.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.speech_to_text.v1.model.SpeechRecognitionResults;

import org.json.*;

public class App {
    private static Preferences prefs;

    private static void watsonTranscribe (String[] creds, JFrame mainframe) {
        // gather input file information
        File audioFile = filePrompt("Select location of input audio file");

        // populate output file
        File textFile = filePrompt("Select location of output text file");

        // when 'cancel' is pressed, or some other fatal error
        if(textFile == null) {
            return;
        }

        // when 'cancel' is pressed, or some other fatal error
        if (audioFile == null) {
            return;
        }

        // do the actual transcription
        IamOptions auth = new IamOptions.Builder()
                .apiKey(creds[0])
                .build();

        SpeechToText stt = new SpeechToText(auth);
        stt.setEndPoint(creds[1]);

        JSONObject recognitionResultsJSON = null;
        String transcription = ""; // result of transcription, returned by function

        String filetype = "audio/";
        String filename = audioFile.getName();
        filetype += filename.substring(filename.lastIndexOf('.') + 1);

        try {
            RecognizeOptions recognizeOptions = new RecognizeOptions.Builder()
                    .audio(new FileInputStream(audioFile))
                    .contentType(filetype)
                    .model("en-US_BroadbandModel")
                    .smartFormatting(true)
                    .build();

            SpeechRecognitionResults recognitionResults =
                    stt.recognize(recognizeOptions).execute().getResult();

            recognitionResultsJSON = new JSONObject(recognitionResults);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }

        // extract transcriptions from JSON response
        JSONArray results = recognitionResultsJSON.getJSONArray("results");

        for (int i = 0; i < results.length(); i++) {
            transcription += results
                    .getJSONObject(i)
                    .getJSONArray("alternatives")
                    .getJSONObject(0)
                    .getString("transcript");
            transcription += "\n";
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(textFile));
            writer.write(transcription);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        JOptionPane.showMessageDialog(mainframe, "Transcription complete!");
    }


    private static File filePrompt (String title) {
        JFileChooser fileBrowser = new JFileChooser();
        fileBrowser.setDialogTitle(title);

        File selectedFile = null;

        if (fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileBrowser.getSelectedFile();
        }

        return selectedFile;
    }


    private static void loadCreds (String[] credentials, Preferences prefs) {
        credentials[0] = "";
        credentials[1] = "";

        String key = prefs.get("api",""); // get API key
        String url = prefs.get("url", ""); // get entry URL

        if (key.length() >= 26 && key.substring(0, 26).equals("SPEECH_TO_TEXT_IAM_APIKEY=")) {
            credentials[0] = key.substring(26);
        }

        if (url.length() >= 19 && url.substring(0, 19).equals("SPEECH_TO_TEXT_URL=")) {
            credentials[1] = url.substring(19);
        }

        // whatever was loaded was not valid
        if (credentials[0].length() == 0 || credentials[1].length() == 0) {
            askCreds(credentials, prefs);
        }
    }


    private static void askCreds (String[] credentials, Preferences prefs) {
        JPanel credsPane = new JPanel();
        JPasswordField keyField = new JPasswordField(45);
        JLabel keyLabel = new JLabel("API Key: ");
        keyLabel.setLabelFor(keyField);

        JTextField urlField = new JTextField(55);
        JLabel urlLabel = new JLabel("Entry URL: ");
        urlLabel.setLabelFor(urlField);

        credsPane.add(keyLabel);
        credsPane.add(keyField);
        credsPane.add(urlLabel);
        credsPane.add(urlField);

        JOptionPane.showConfirmDialog(null, credsPane, "Enter your API key", JOptionPane.OK_CANCEL_OPTION);

        credentials[0] = new String(keyField.getPassword());
        credentials[1] = urlField.getText();

        // set prefs, must be done here to accommodate "Update API key" button
        prefs.put("api", "SPEECH_TO_TEXT_IAM_APIKEY=" + credentials[0]);
        prefs.put("url", "SPEECH_TO_TEXT_URL=" + credentials[1]);
    }


    public static void main (String[] args) {
        prefs = Preferences.userNodeForPackage(App.class);
        String[] credentials = new String[2];

        loadCreds(credentials, prefs);

        JFrame mainframe = new JFrame("Welcome to Watson STT");
        mainframe.setSize(600,200);
        mainframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel topPane = new JPanel();
        topPane.setLayout(new BoxLayout(topPane, BoxLayout.Y_AXIS));
        JPanel bottomPane = new JPanel();
        mainframe.add(topPane);
        mainframe.add(bottomPane, BorderLayout.PAGE_END);

        JLabel title = new JLabel("Welcome to IBM Watson Speech To Text.");
        title.setFont(new Font("Arial", Font.BOLD, 20));
        title.setAlignmentX(Component.CENTER_ALIGNMENT);
        topPane.add(title);

        JLabel body = new JLabel(
            "Your IBM Watson API key has been loaded, please select an option below."
        );
        body.setFont(new Font("Arial", Font.PLAIN, 14));
        body.setAlignmentX(Component.CENTER_ALIGNMENT);
        topPane.add(body);

        JButton scribeButton = new JButton("Transcribe");
        JButton helpButton = new JButton("Help");
        JButton apiButton = new JButton("Update API Key");
        JButton exitButton = new JButton("Exit");

        String[] finalCredentials = credentials;
        scribeButton.addActionListener(actionEvent -> watsonTranscribe(finalCredentials, mainframe));

        // help dialog string for helpButton
        String helpText = "There are only two fundamental functions in this application:\n"
            + "1. Setting the API key and URL route for IBM Watson\n"
            + "2. Sending an audio file to the account associated with the API key and outputting a text file.\n\n"
            + "The first of these shouldn't need to be used very often, because the application is aware of whether\n"
            + "an api key has been previously provided. If you need to change it, click the \"Update API Key\"\n"
            + "button and supply the new key and URL.\n\n"
            + "To transcribe a file, start by clicking \"Transcribe\". You will first be prompted to first select\n"
            + "a valid audio file from your file system, and then a text file to output to. If you wish to create a\n"
            + "new text file instead of overwriting an existing one, navigate to the directory you would like\n"
            + "the new file to be created in, and define a new file name in the field below. This will create the\n"
            + "text file when you click \"Open\", and will populate it with the transcription once it completes.\n\n"
            + "There will be a pause while IBM Watson transcribes your input audio file. The longer the\n"
            + "audio file is, the longer this process will take. When the transcription is complete, a dialog box\n"
            + "will appear to inform you. DO NOT close the program before this dialog appears, or no part\n"
            + "of the transcription will be saved.\n\n\n"
            + "If you're interested in support or installation for this app, please contact:\n"
            + "Parker Ammar: parker.ammar@gmail.com";
        helpButton.addActionListener(actionEvent -> JOptionPane.showMessageDialog(mainframe, helpText));


        apiButton.addActionListener(actionEvent -> askCreds(credentials, prefs));
        exitButton.addActionListener(actionEvent -> System.exit(0));

        bottomPane.add(scribeButton);
        bottomPane.add(apiButton);
        bottomPane.add(helpButton);
        bottomPane.add(exitButton);

        mainframe.setVisible(true);
    }
}