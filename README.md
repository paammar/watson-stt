# Watson Speech To Text
IBM Cloud does not offer a straightforward application for using Watson's
speech to text capability. This application provides a GUI which allows the
user to select a file, send it to Watson, and then save the transcription to a
text file.

## Usage
This code can be compiled into an executable JAR file, which will in turn 
handle any requisite setup. If you want help with this process, I'm more than
happy to sell you some of my time.

## License
See [here](https://gitlab.com/paammar/watson-stt/blob/master/LICENSE).

## Authors
Parker Ammar